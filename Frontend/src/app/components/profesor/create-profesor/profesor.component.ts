import { Component, OnInit, HostBinding } from '@angular/core';
import { Profesor } from 'src/app/models/profesor';
import { ProfesorService } from 'src/app/services/profesor.service';
import { Router, ActivatedRoute } from '@angular/router';
import {BehaviorSubject} from 'rxjs';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-profesor',
  templateUrl: './profesor.component.html',
  styleUrls: ['./profesor.component.css']
})
export class ProfesorComponent implements OnInit {

  @HostBinding('class') clases = 'center';

  profesor: Profesor = {
    profesorId: 0,
    nombres: '',
    apellidos: '',
    email: '',
    dni: '',
    usuario: '',
    password: ''
  };

  edit: boolean = false;

  constructor(private profesorService: ProfesorService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {

    const params = this.activatedRoute.snapshot.params;
    if (params.profesorId) {
      this.profesorService.getProfesor(params.profesorId)
        .subscribe(
          res => {
            console.log(res);
            this.profesor = res;
            this.edit = true;
          },
          err => console.log(err)
        )
    }
  }

  saveNewProfesor() {
    delete this.profesor.profesorId;
    this.profesorService.saveProfesor(this.profesor)
      .subscribe(
        res => {
          console.log(res);
          this.router.navigate(['/principal']);
        },
        err => console.error(err)
      )
  }

  updateProfesor() {
    this.profesorService.updateProfesor(this.profesor.profesorId, this.profesor)
      .subscribe(
        res => { 
          console.log(res);
          this.router.navigate(['/principal']);
        },
        err => console.error(err)
      )
  }
}