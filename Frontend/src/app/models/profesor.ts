import { Title } from '@angular/platform-browser';

export interface Profesor {
    profesorId?: number,
    nombres?: string,
    apellidos?: string,
    email?: string,
    dni?: string,
    usuario?: string,
    password?: string
};