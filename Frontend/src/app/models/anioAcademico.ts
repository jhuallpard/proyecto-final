import { Title } from '@angular/platform-browser';

export interface AnioAcademico {
    anioAcademicoId?: number,
    nombre?: string,
    orden?: string
};