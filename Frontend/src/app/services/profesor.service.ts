import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Profesor } from '../models/profesor';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProfesorService {

  API_URI = 'http://localhost:3000/api';

  constructor(private http: HttpClient) { }

  getProfesores() {
    return this.http.get(`${this.API_URI}/profesor`);
  }

  getProfesor(profesorId: string) {
    return this.http.get(`${this.API_URI}/profesor/${profesorId}`);
  }

  deleteProfesor(profesorId: string) {
    return this.http.delete(`${this.API_URI}/profesor/${profesorId}`);
  }

  saveProfesor(profesor: Profesor) {
    return this.http.post(`${this.API_URI}/profesor`, profesor);
  }

  updateProfesor(profesorId: undefined|number, updateProfesor: Profesor): Observable<Profesor> {
    return this.http.put(`${this.API_URI}/profesor/${profesorId}`, updateProfesor);
  }
}
