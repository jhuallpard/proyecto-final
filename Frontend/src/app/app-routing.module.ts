import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { LocalComponent } from './components/local/create-local/local.component';
import { ProfesorComponent } from './components/profesor/create-profesor/profesor.component';
import { ListProfesorComponent } from './components/profesor/list-profesor/list-profesor.component';

const routes: Routes = [
  {path: 'principal', component: AppComponent},
  {path: 'profesor', component: ProfesorComponent},
  {path: 'listProfesor', component: ListProfesorComponent},
  {path: 'local', component: LocalComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
