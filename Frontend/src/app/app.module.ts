import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule} from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { LocalComponent } from './components/local/create-local/local.component';
import { ProfesorComponent } from './components/profesor/create-profesor/profesor.component';
import { ListProfesorComponent } from './components/profesor/list-profesor/list-profesor.component';
import { ListLocalComponent } from './components/local/list-local/list-local.component';

@NgModule({
  declarations: [
    AppComponent,
    LocalComponent,
    ProfesorComponent,
    ListProfesorComponent,
    ListLocalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
