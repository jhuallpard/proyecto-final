import { Request, Response } from 'express';
import pool from '../database';

class LocalController {

    public async list(req: Request, res: Response): Promise<void> {
        const local = await pool.query('SELECT * FROM local');
        res.json(local);
    }

    public async getOne(req: Request, res: Response): Promise<any> {
        const { localId } = req.params;
        const local = await pool.query('SELECT * FROM local WHERE localId = ?', [localId]);
        console.log(local.length);
        if (local.length > 0) {
            return res.json(local[0]);
        }
        res.status(404).json({ text: "El Local no existe" });
    }

    public async create(req: Request, res: Response): Promise<void> {
        const result = await pool.query('INSERT INTO local set ?', [req.body]);
        res.json({ message: 'Guardar Local' });
    }

    public async update(req: Request, res: Response): Promise<void> {
        const { localId } = req.params;
        const oldLocal = req.body;
        await pool.query('UPDATE local set ? WHERE localId = ?', [req.body, localId]);
        res.json({ message: "El Local a sido actualizado" });
    }

    public async delete(req: Request, res: Response): Promise<void> {
        const { localId } = req.params;
        await pool.query('DELETE FROM local WHERE localId = ?', [localId]);
        res.json({ message: "El Local ha sido eliminado" });
    }
}

const localController = new LocalController;
export default localController;