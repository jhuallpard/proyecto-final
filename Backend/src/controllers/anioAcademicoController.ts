import { Request, Response } from 'express';
import pool from '../database';

class AnioAcademicoController {

    public async list(req: Request, res: Response): Promise<void> {
        const trabajos = await pool.query('SELECT * FROM anio_academico');
        res.json(trabajos);
    }

    public async getOne(req: Request, res: Response): Promise<any> {
        const { anioAcademicoId } = req.params;
        const anioAcademico = await pool.query('SELECT * FROM anio_academico', [anioAcademicoId]);
        console.log(anioAcademico.length);
        if (anioAcademico.length > 0) {
            return res.json(anioAcademico[0]);
        }
        res.status(404).json({ text: "El Anio Academico no existe" });
    }

    public async create(req: Request, res: Response): Promise<void> {
        const result = await pool.query('INSERT INTO anio_academico set ?', [req.body]);
        res.json({ message: 'Guardar Trabajo' });
    }

    public async update(req: Request, res: Response): Promise<void> {
        const { anioAcademicoId } = req.params;
        const oldanioAcademico = req.body;
        await pool.query('UPDATE anio_academico set ? WHERE anioAcademicoId = ?', [req.body, anioAcademicoId]);
        res.json({ message: "El Anio Academico a sido actualizado" });
    }

    public async delete(req: Request, res: Response): Promise<void> {
        const { anioAcademicoId } = req.params;
        await pool.query('DELETE FROM anio_academico WHERE anioAcademicoId = ?', [anioAcademicoId]);
        res.json({ message: "El Anio Academico ha sido eliminado" });
    }
}

const anioAcademicoController = new AnioAcademicoController;
export default anioAcademicoController;