import { Request, Response } from 'express';
import pool from '../database';

class ProfesorController {

    public async list(req: Request, res: Response): Promise<void> {
        const profesor = await pool.query('SELECT * FROM profesor');
        res.json(profesor);
    }

    public async getOne(req: Request, res: Response): Promise<any> {
        const { profesorId } = req.params;
        const profesor = await pool.query('SELECT * FROM profesor WHERE profesorId = ?', [profesorId]);
        console.log(profesor.length);
        if (profesor.length > 0) {
            return res.json(profesor[0]);
        }
        res.status(404).json({ text: "El Profesor no existe" });
    }

    public async create(req: Request, res: Response): Promise<void> {
        const result = await pool.query('INSERT INTO profesor set ?', [req.body]);
        res.json({ message: 'Guardar Profesor' });
    }

    public async update(req: Request, res: Response): Promise<void> {
        const { profesorId } = req.params;
        const oldProfesor = req.body;
        await pool.query('UPDATE profesor set ? WHERE profesorId = ?', [req.body, profesorId]);
        res.json({ message: "El Profesor a sido actualizado" });
    }

    public async delete(req: Request, res: Response): Promise<void> {
        const { profesorId } = req.params;
        await pool.query('DELETE FROM profesor WHERE profesorId = ?', [profesorId]);
        res.json({ message: "El Profesor ha sido eliminado" });
    }
}

const profesorController = new ProfesorController;
export default profesorController;