import { Request, Response } from 'express';

class IndexController {

    public index(req: Request, res: Response) {
        res.send('Backend Horarios');
    }
}

export const indexController = new IndexController;