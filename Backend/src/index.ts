import express, { Application } from 'express';

import morgan from 'morgan';
import cors from 'cors';

import indexRoutes from './routes/indexRoutes';
import ProfesorRoutes from './routes/profesorRoutes';
import AnioAcademicoRoutes from './routes/anioAcademicoRoutes';
import LocalRoutes from './routes/localRoutes';
import LoginRoutes from './routes/loginRoutes';

class Server{

	public app: Application;

		constructor(){
			this.app = express();
			this.config();
	        this.routes();
		}

	config(): void {
		this.app.set('port', process.env.PORT || 3000);
		this.app.use(morgan('dev'));
	    this.app.use(cors());
	    this.app.use(express.json());
	    this.app.use(express.urlencoded({extended: false}));

	}
	routes():void {
		this.app.use('/', indexRoutes);
		this.app.use('/api/', LoginRoutes);
	    this.app.use('/api/anioAcademico', AnioAcademicoRoutes);
	    this.app.use('/api/profesor', ProfesorRoutes);
	    this.app.use('/api/local', LocalRoutes);
	}
	start(): void {
		this.app.listen(this.app.get('port'), () => {
	            console.log('Server on port', this.app.get('port'));
	        });
	}
}

const server = new Server();
server.start();
