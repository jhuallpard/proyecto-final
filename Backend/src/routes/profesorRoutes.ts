import { Router } from 'express';
import ProfesorController from '../controllers/profesorController';


class ProfesorRoutes {

    public router: Router = Router();

    constructor() {
        this.config();
    }

    config(): void {
        this.router.get('/', ProfesorController.list);
        this.router.get('/:profesorId', ProfesorController.getOne);
        this.router.post('/', ProfesorController.create);
        this.router.put('/:profesorId', ProfesorController.update);
        this.router.delete('/:profesorId', ProfesorController.delete);
    }

}

const profesorRoutes = new ProfesorRoutes();
export default profesorRoutes.router;