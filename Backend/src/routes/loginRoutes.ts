import { Router } from 'express';
import LoginController from '../controllers/loginController';


class LoginRoutes {

    public router: Router = Router();

    constructor() {
        this.config();
    }

    config(): void {
        this.router.post('/login', LoginController.login);
    }

}

const loginRoutes = new LoginRoutes();
export default loginRoutes.router;