import { Router } from 'express';
import AnioAcademicoController from '../controllers/anioAcademicoController';


class AnioAcademicoRoutes {

    public router: Router = Router();

    constructor() {
        this.config();
    }

    config(): void {
        this.router.get('/', AnioAcademicoController.list);
        this.router.get('/:anioAcademicoId', AnioAcademicoController.getOne);
        this.router.post('/', AnioAcademicoController.create);
        this.router.put('/:anioAcademicoId', AnioAcademicoController.update);
        this.router.delete('/:anioAcademicoId', AnioAcademicoController.delete);
    }
}

const anioAcademicoRoutes = new AnioAcademicoRoutes();
export default anioAcademicoRoutes.router;