import { Router } from 'express';
import LocalController from '../controllers/localController';


class LocalRoutes {

    public router: Router = Router();

    constructor() {
        this.config();
    }

    config(): void {
        this.router.get('/', LocalController.list);
        this.router.get('/:localId', LocalController.getOne);
        this.router.post('/', LocalController.create);
        this.router.put('/:localId', LocalController.update);
        this.router.delete('/:localId', LocalController.delete);
    }

}

const localRoutes = new LocalRoutes();
export default localRoutes.router;