"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = __importDefault(require("../database"));
class ProfesorController {
    list(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const profesor = yield database_1.default.query('SELECT * FROM profesor');
            res.json(profesor);
        });
    }
    getOne(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { profesorId } = req.params;
            const profesor = yield database_1.default.query('SELECT * FROM profesor WHERE profesorId = ?', [profesorId]);
            console.log(profesor.length);
            if (profesor.length > 0) {
                return res.json(profesor[0]);
            }
            res.status(404).json({ text: "El Profesor no existe" });
        });
    }
    create(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield database_1.default.query('INSERT INTO profesor set ?', [req.body]);
            res.json({ message: 'Guardar Profesor' });
        });
    }
    update(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { profesorId } = req.params;
            const oldProfesor = req.body;
            yield database_1.default.query('UPDATE profesor set ? WHERE profesorId = ?', [req.body, profesorId]);
            res.json({ message: "El Profesor a sido actualizado" });
        });
    }
    delete(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { profesorId } = req.params;
            yield database_1.default.query('DELETE FROM profesor WHERE profesorId = ?', [profesorId]);
            res.json({ message: "El Profesor ha sido eliminado" });
        });
    }
}
const profesorController = new ProfesorController;
exports.default = profesorController;
