"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = __importDefault(require("../database"));
class LocalController {
    list(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const local = yield database_1.default.query('SELECT * FROM local');
            res.json(local);
        });
    }
    getOne(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { localId } = req.params;
            const local = yield database_1.default.query('SELECT * FROM local WHERE localId = ?', [localId]);
            console.log(local.length);
            if (local.length > 0) {
                return res.json(local[0]);
            }
            res.status(404).json({ text: "El Local no existe" });
        });
    }
    create(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield database_1.default.query('INSERT INTO local set ?', [req.body]);
            res.json({ message: 'Guardar Local' });
        });
    }
    update(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { localId } = req.params;
            const oldLocal = req.body;
            yield database_1.default.query('UPDATE local set ? WHERE localId = ?', [req.body, localId]);
            res.json({ message: "El Local a sido actualizado" });
        });
    }
    delete(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { localId } = req.params;
            yield database_1.default.query('DELETE FROM local WHERE localId = ?', [localId]);
            res.json({ message: "El Local ha sido eliminado" });
        });
    }
}
const localController = new LocalController;
exports.default = localController;
