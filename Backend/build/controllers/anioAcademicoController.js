"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = __importDefault(require("../database"));
class AnioAcademicoController {
    list(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const trabajos = yield database_1.default.query('SELECT * FROM anio_academico');
            res.json(trabajos);
        });
    }
    getOne(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { anioAcademicoId } = req.params;
            const anioAcademico = yield database_1.default.query('SELECT * FROM anio_academico', [anioAcademicoId]);
            console.log(anioAcademico.length);
            if (anioAcademico.length > 0) {
                return res.json(anioAcademico[0]);
            }
            res.status(404).json({ text: "El Anio Academico no existe" });
        });
    }
    create(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield database_1.default.query('INSERT INTO anio_academico set ?', [req.body]);
            res.json({ message: 'Guardar Trabajo' });
        });
    }
    update(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { anioAcademicoId } = req.params;
            const oldanioAcademico = req.body;
            yield database_1.default.query('UPDATE anio_academico set ? WHERE anioAcademicoId = ?', [req.body, anioAcademicoId]);
            res.json({ message: "El Anio Academico a sido actualizado" });
        });
    }
    delete(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { anioAcademicoId } = req.params;
            yield database_1.default.query('DELETE FROM anio_academico WHERE anioAcademicoId = ?', [anioAcademicoId]);
            res.json({ message: "El Anio Academico ha sido eliminado" });
        });
    }
}
const anioAcademicoController = new AnioAcademicoController;
exports.default = anioAcademicoController;
