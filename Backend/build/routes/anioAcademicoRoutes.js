"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const anioAcademicoController_1 = __importDefault(require("../controllers/anioAcademicoController"));
class AnioAcademicoRoutes {
    constructor() {
        this.router = (0, express_1.Router)();
        this.config();
    }
    config() {
        this.router.get('/', anioAcademicoController_1.default.list);
        this.router.get('/:anioAcademicoId', anioAcademicoController_1.default.getOne);
        this.router.post('/', anioAcademicoController_1.default.create);
        this.router.put('/:anioAcademicoId', anioAcademicoController_1.default.update);
        this.router.delete('/:anioAcademicoId', anioAcademicoController_1.default.delete);
    }
}
const anioAcademicoRoutes = new AnioAcademicoRoutes();
exports.default = anioAcademicoRoutes.router;
