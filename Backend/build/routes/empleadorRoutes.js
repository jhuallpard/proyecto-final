"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const empleadorController_1 = __importDefault(require("../controllers/empleadorController"));
class EmpleadorRoutes {
    constructor() {
        this.router = (0, express_1.Router)();
        this.config();
    }
    config() {
        this.router.get('/', empleadorController_1.default.list);
        this.router.get('/:id', empleadorController_1.default.getOne);
        this.router.post('/', empleadorController_1.default.create);
        this.router.put('/:id', empleadorController_1.default.update);
        this.router.delete('/:id', empleadorController_1.default.delete);
    }
}
const empleadorRoutes = new EmpleadorRoutes();
exports.default = empleadorRoutes.router;
