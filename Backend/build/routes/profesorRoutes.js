"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const profesorController_1 = __importDefault(require("../controllers/profesorController"));
class ProfesorRoutes {
    constructor() {
        this.router = (0, express_1.Router)();
        this.config();
    }
    config() {
        this.router.get('/', profesorController_1.default.list);
        this.router.get('/:profesorId', profesorController_1.default.getOne);
        this.router.post('/', profesorController_1.default.create);
        this.router.put('/:profesorId', profesorController_1.default.update);
        this.router.delete('/:profesorId', profesorController_1.default.delete);
    }
}
const profesorRoutes = new ProfesorRoutes();
exports.default = profesorRoutes.router;
