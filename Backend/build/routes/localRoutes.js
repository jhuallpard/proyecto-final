"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const localController_1 = __importDefault(require("../controllers/localController"));
class LocalRoutes {
    constructor() {
        this.router = (0, express_1.Router)();
        this.config();
    }
    config() {
        this.router.get('/', localController_1.default.list);
        this.router.get('/:localId', localController_1.default.getOne);
        this.router.post('/', localController_1.default.create);
        this.router.put('/:localId', localController_1.default.update);
        this.router.delete('/:localId', localController_1.default.delete);
    }
}
const localRoutes = new LocalRoutes();
exports.default = localRoutes.router;
